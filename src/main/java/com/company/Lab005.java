package com.company;



//Zad 1. Utworzyć losową macierz zerojedynkową n x k (n, k z przedziału <4,8>).
// Znaleźć największą liczbę jedynek w wierszu lub kolumnie lub przekątnej (jeśli to macierz kwadratowa)

//Zad 2. Wygenerować tablicę poszarpaną (jagged array) złożoną z cyfr w n wierszach i maksymalnie k kolumnach
// (n, k z przedziału <4,8>) – w każdym wierszu od 3 od k elementów (losowa ilość).
// Ile wynosi średnia suma elementów w wierszu i ile jest wierszy o sumie większej od tej średniej.

// Zad 3. Napisać metodę do mnożenia macierzy. Na początku sprawdzać czy mnożenie może być wykonane.

//Zad 4. Napisać metodę D’Hondta (obowiązującą w Polsce) do rozdzielania liczby mandatów
// w wyborach https://pl.wikipedia.org/wiki/Metoda_D%E2%80%99Hondta

import java.util.*;

class Matrix{
    int row;
    int col;
    int array[][];
    Matrix(int row, int col,int [][] myArray){
        this.row = row;
        this.col = col;
        array = myArray.clone();

    }

    public Matrix(int row, int col) {
        this.row = row;
        this.col = col;
        array = new int[row][col];
    }

    public void multiplication(Matrix B){
        if(this.col != B.row ){
            System.out.println("Mnozenie nie moze byc wykonane ");
        }else{
            Matrix result = new Matrix(this.row,B.col);
            for(int i=0;i< this.row;i++){
                for(int j =0;j< B.col;j++){
                    for(int k =0;k< B.row;k++) {
                        result.array[i][j] += this.array[i][k] * B.array[k][j];
                    }
                    System.out.print(result.array[i][j]+" ");
                }
                System.out.println();
            }
        }
    }
}


public class Lab005 {
    Random random = new Random();

    public void zad1(){
        int n = random.nextInt(9-4)+4;
        int k = random.nextInt(9-4)+4;
        int tab[][] = new int [n][k];

        int max_value_row=0;
        int max_value_col=0;
        int tmp_row=0;
        int tmp_col=0;
        int diagonal1=0;
        int diagonal2=0;
        int max_diagonal=0;

        for(int i=0;i<n;i++){
            for(int j=0;j<k;j++){
                tab[i][j] = random.nextInt(2-0);
                if(tab[i][j]==1){
                    tmp_row++;
                }
            }
            if(tmp_row>max_value_row)
                max_value_row=tmp_row;
            tmp_row=0;
        }

        for(int j=0;j<k;j++){
            for(int i=0;i<n;i++){
                if(tab[i][j]==1){
                    tmp_col++;
                }
            }
            if(tmp_col>max_value_col)
                max_value_col=tmp_col;
            tmp_col=0;
        }


        if(n==k){
            int j=n-1;
            for(int i=0;i<n;i++){
               if(tab[i][i]==1){
                    diagonal1++;
               }
               if (tab[i][j]==1){
                   diagonal2++;
               }
               j--;
            }
            if(diagonal1>=diagonal2)
                max_diagonal = diagonal1;
            if (diagonal2>=diagonal1)
                max_diagonal = diagonal2;
            System.out.println("Wiersze "+max_value_row+" Kolumny "+max_value_col+" przekątne "+max_diagonal);
        }else{
            System.out.println("Wiersze "+max_value_row+" Kolumny "+max_value_col);
        }


    }

//Zad 2. Wygenerować tablicę poszarpaną (jagged array) złożoną z cyfr w n wierszach i maksymalnie k kolumnach
// (n, k z przedziału <4,8>) – w każdym wierszu od 3 od k elementów (losowa ilość).
// Ile wynosi średnia suma elementów w wierszu i ile jest wierszy o sumie większej od tej średniej.
    public void zad2(){
        int n=random.nextInt(9-4)+4;

        int tab[][] = new int[n][];

        for(int i=0;i<tab.length;i++){
            int k =random.nextInt(9-3)+3;
            tab[i] = new int[k];


            for(int j=0;j<k;j++){
                tab[i][j] = random.nextInt(10);
                System.out.print(tab[i][j] +" ");
            }
            System.out.println();
        }

        double suma_wiersza = 0;
        int counter=0;
        double srednia_wierszy=0;
        ArrayList<Double> lista = new ArrayList<>();

        for(int i= 0;i < tab.length;i++){
            for (int j=0;j<tab[i].length;j++){
                suma_wiersza = suma_wiersza +tab[i][j];
            }
            lista.add(suma_wiersza);
            srednia_wierszy = srednia_wierszy+suma_wiersza;
            suma_wiersza=0;
        }
        srednia_wierszy = srednia_wierszy /n;

        for(int i=0;i<lista.size();i++){
            if(lista.get(i)>srednia_wierszy){
                counter++;
            }
        }
        System.out.println("srednia suma elemtnow w wierszu "+srednia_wierszy +" ile wierszy o sumie wiekszej od tej sredniej "+counter);

    }

// Zad 3. Napisać metodę do mnożenia macierzy. Na początku sprawdzać czy mnożenie może być wykonane.
    public void zad3(){
        //tu tylko sprawdzam czy działa
        int a[][] = { { 5, -1, 0 },
                { 4, 9, 4 },
                { -10, 0, 7 },
                { 1, 2, 3 } };

        int b[][] = { { 1, -5, 5 },
                { 6, -2, 1 },
                { 2, 13, -3 } };

        Matrix A = new Matrix(4,3,a);
        Matrix B = new Matrix(3,3,b);
        //metoda do obliczen w klasie Matrix
        A.multiplication(B);

    }

//Zad 4. Napisać metodę D’Hondta (obowiązującą w Polsce) do rozdzielania liczby mandatów
// w wyborach https://pl.wikipedia.org/wiki/Metoda_D%E2%80%99Hondta

    //implementujemy interfejs Comparable aby moc nadpisac metode compareTo to ułatwi nam sortowanie listy
class dhondt implements Comparable<dhondt>{
       private int id;
       private int number_votes;

    dhondt(int id, int number_votes){
        this.id = id;
        this.number_votes = number_votes;

    }

    public int getId() {
        return id;
    }


    public int getNumber_votes() {
        return number_votes;
    }

    @Override
    public int compareTo(dhondt o) {
        return Integer.compare(this.getNumber_votes(),o.getNumber_votes());
    }
}

    public void zad4(int numberOfMandate){
        int numberCommittee=5; //ustawione na sztywno  bez wczytywania
        ArrayList<dhondt> listDhondt = new ArrayList<>();
        ArrayList<dhondt> resultList = new ArrayList<>();
//         Wczytywanie
//        Scanner input = new Scanner(System.in);
//        System.out.println("Liczba komitetow");
//        numberCommittee = input.nextInt();
//        //zakladam ze glosy sa wazne
//        for(int i=0;i<numberCommittee;i++){
//            System.out.println("Podaj liczbe waznycy glosow dla "+i+" okregu");
//            listDhondt.add(new dhondt(i,input.nextInt()));
//        }
        //ustawione na sztwno do testow  bez wczytywania prosze zmienic tez numberCommitte przy dodaniu lub usuwaniu obiektow
        listDhondt.add(new dhondt(0,4000));
        listDhondt.add(new dhondt(1,3000));
        listDhondt.add(new dhondt(2,2000));
        listDhondt.add(new dhondt(3,1000));
        listDhondt.add(new dhondt(4,5000));

        for(int i=0;i<numberCommittee;i++){
            for(int j=1;j<=numberOfMandate;j++){
                resultList.add(new dhondt(listDhondt.get(i).getId(),listDhondt.get(i).getNumber_votes()/j));
            }
        }
        Collections.sort(resultList, Collections.reverseOrder());
        int[] numberMandateResultByCommitte = new int[listDhondt.size()];
        Arrays.fill(numberMandateResultByCommitte,0);
        int i=0;
        while(i<numberOfMandate){
            //gdy kolejne ilorzay maja taka sama wartosc sparwdzamy zgodnie  z systemem w Polsce sparwdzamy ile glosow oddano w danym okregu
            //Brzydki kod ale "U mnie działa"
            if(resultList.get(i).getNumber_votes() == resultList.get(numberOfMandate).getNumber_votes()){
                int j=i;
                int maxVotes;
                int maxid=listDhondt.get(resultList.get(j).getId()).getId();
                while((j+1)<resultList.size() && resultList.get(j).getNumber_votes() == resultList.get(j+1).getNumber_votes() ){
                    maxVotes = listDhondt.get(resultList.get(j).getId()).getNumber_votes();
                    if(maxVotes<=listDhondt.get(resultList.get(j+1).getId()).getNumber_votes()){
                        maxVotes = listDhondt.get(resultList.get(j+1).getId()).getNumber_votes();
                        maxid = listDhondt.get(resultList.get(j+1).getId()).getId();
                    }
                    j++;
                }
                numberMandateResultByCommitte[maxid] = numberMandateResultByCommitte[maxid]+1;
                i++;
            }else{
                numberMandateResultByCommitte[listDhondt.get(resultList.get(i).getId()).getId()]=numberMandateResultByCommitte[listDhondt.get(resultList.get(i).getId()).getId()]+1;
                i++;
            }
        }

        //resultList.forEach((dhondt -> System.out.println(dhondt.getId() +" "+ dhondt.getNumber_votes())));

        System.out.println();

        for(int a=0;a<numberMandateResultByCommitte.length;a++){
            System.out.println("Komitet o id: "+a+" liczba mandatow "+ numberMandateResultByCommitte[a]);
        }

    }

}

